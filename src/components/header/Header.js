// Header.js

import React from "react";

const Header = () => (
  <header>
    <h1>Products</h1>
  </header>
);

export default Header;
