import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Header from "./components/header/Header";
import Product from './components/Product';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Grid container justify = "center">
          < Product />
      </Grid>
      </div>
    );
  }
}

export default App;
